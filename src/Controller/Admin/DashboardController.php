<?php

namespace App\Controller\Admin;
 
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use App\Entity\Haircut;
use App\Entity\User;
use App\Entity\Event;

/**
 * @codeCoverageIgnore 
 */
class DashboardController extends AbstractDashboardController
{ 
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    } 

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Haircut');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Coupe de cheveux', 'fas fa-newspaper', Haircut::class);
        yield MenuItem::linkToCrud('Membre du site', 'fas fa-newspaper', User::class);
        yield MenuItem::linkToCrud('RDV', 'fas fa-newspaper', Event::class);
    }
}
