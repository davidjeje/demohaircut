<?php

namespace App\Controller\Admin; 

use App\Entity\Haircut;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

/**
 * @codeCoverageIgnore 
 */
class HaircutCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Haircut::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
