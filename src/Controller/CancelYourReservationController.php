<?php

namespace App\Controller;  

use App\Repository\EventRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;

class CancelYourReservationController extends AbstractController
{ 
	/** 
     * @Route("/user/deletion/reservation/date", name="event_planning_delete", methods={"GET"})
     */
    public function deletionOfTheReservationDate(EventRepository $eventRepository)
    {
        $user = $this->getUser()->getId();
        
        $rdvMembers = $eventRepository->findBy(['member' => $user]);
        
        $rdvs = [];

        foreach ($rdvMembers as $rdvMember ) {
            $rdvs[] = [
                
                'title' => $rdvMember->getTitle(),
                'start' => $rdvMember->getStart()->format("Y-m-d H:i:s"),
                'end' => $rdvMember->getEnd()->format("Y-m-d H:i:s")
            ];
        }

        $data = json_encode($rdvs);

        return $this->render('cancelYourReservation/deletionOfTheReservationDate.html.twig', [
            'datas' => $data 
        ]);
    }

    /**
     * @Route("/user/event/data/to/delete", name="event_data_to_delete", methods={"GET|POST"})
     */
    public function eventDataToDelete(EventRepository $eventRepository, Request $request)
    { 
        $startDateEvent = new \dateTime($_GET['start']);
        
        $endDateEvent =  new \dateTime($_GET['end']);
        
        $event = $eventRepository->findOneBy([
            'member' => $this->getUser()->getId(),
            'start' => $startDateEvent,
            'end' => $endDateEvent
        ]);
 
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($event);
        $entityManager->flush();

        return $this->redirectToRoute('event_success_delete');
    }

    /**
     * @Route("/user/event/success/delete", name="event_success_delete", methods={"GET|POST"})
     */
    public function eventSuccessDelete(EventRepository $eventRepository)
    {
        $this->addFlash('success', ' L\'annulation de votre RDV est réussite !!! ');

        return $this->redirectToRoute('home_page');
    } 
}