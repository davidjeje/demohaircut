<?php

namespace App\Controller; 

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\UsType; 

class ChangeNumberController extends AbstractController
{
	/**
     * @Route("/user/update/number", name="update_number", methods={"GET|POST"}) 
     */
    public function updateNumberUser(Request $request): Response
    {
        $user = $this->getUser();
        
        $form = $this->createForm(UsType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())  {
            
            $number = $user->getNumber();
            
            $user->setNumber($number);  
            
            $orm = $this->getDoctrine()->getManager();
            $orm->persist($user);
            $orm->flush();

            $this->addFlash('success', ' La modification de votre numéro de téléphone est réussite !!! ');

            return $this->redirectToRoute('home_page');
        }

        return $this->render('changeNumber/updateNumberUser.html.twig', ['user' => $user,
            'form' => $form->createView()]); 
    }
} 