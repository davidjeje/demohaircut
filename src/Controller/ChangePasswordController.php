<?php

namespace App\Controller; 

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\UsersType;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;


class ChangePasswordController extends AbstractController
{ 
	/**
     * @Route("/user/update/password", name="update_password", methods={"GET|POST"}) 
     */
    public function updatePassword(Request $request, UserPasswordHasherInterface $passwordEncoder): Response
    {
        $user = $this->getUser(); 
        
        $form = $this->createForm(UsersType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())  {

            $password = $passwordEncoder->hashPassword($user, $user->getPassword());
            $user->setPassword($password);  
            
            $orm = $this->getDoctrine()->getManager();
            $orm->persist($user);
            $orm->flush();

            $this->addFlash('success', ' La modification de votre mot de passe est réussite !!! Dorénavant connectez-vous avec ce mot de passe.');

            return $this->redirectToRoute('home_page');
        } 

        return $this->render('changePassword/updatePassword.html.twig', ['user' => $user,
            'form' => $form->createView()]); 
    } 
} 