<?php

namespace App\Controller; 

use App\Entity\Event; 
use App\Repository\EventRepository;
use App\Entity\Date; 
use App\Repository\DateRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\DateTime;


class ModifyHisBookingController extends AbstractController
{
	 /**
     * @Route("/user/select/booking/date/modify", name="event_planning_update", methods={"GET"})
     */
    public function selectTheBookingDateToModify(EventRepository $eventRepository)
    {
        $user = $this->getUser()->getId();
        //dd($user);
        
        $rdvMembers = $eventRepository->findBy(['member' => $user]);
        //dd($rdvMembers);
        
        $rdvs = [];
 
        foreach ($rdvMembers as $rdvMember ) {
            $rdvs[] = [
                
                'title' => $rdvMember->getTitle(),
                'start' => $rdvMember->getStart()->format("Y-m-d H:i:s"),
                'end' => $rdvMember->getEnd()->format("Y-m-d H:i:s")
            ];
        }
 
        $data = json_encode($rdvs);

        return $this->render('modifyHisBooking/selectTheBookingDateToModify.html.twig', [
            'datas' => $data
        ]);
    } 

    /**
     * @Route("/user/event/give/data", name="event_give_data", methods={"GET|POST"})
     */
    public function dataGive(EventRepository $eventRepository)
    { 
        $start = htmlspecialchars($_GET['start']);
        $end = htmlspecialchars($_GET['end']);
        
        if(isset($start) && !empty($start) && isset($end) && !empty($end)){
            $dateStart = new \dateTime($start);
        
            $dateEnd =  new \dateTime($end);

            $event = $eventRepository->findOneBy(['start' => $dateStart, 'end' => $dateEnd, 'member' => $this->getUser()->getId()]);
            
            if(isset($event) && !empty($event) ){
                $dates = new Date();

                $date = $dates->setStart($event->getStart());
                $date = $dates->setEnd($event->getEnd());
                $date = $dates->setMember($this->getUser());
                
                $orm = $this->getDoctrine()->getManager();
                $orm->persist($date);
                $orm->flush();

                return $this->redirectToRoute('event_change');
            }
        }
    }

    /**
     * @Route("/user/event/change", name="event_change", methods={"GET|POST"})
     */
    public function change(EventRepository $eventRepository, DateRepository $dateRepository)
    {
        $dateLast = $dateRepository->findOneBy(['member' => $this->getUser()->getId()], ['id' => 'desc']);
        $dateFirst = $dateRepository->findOneBy(['member' => $this->getUser()->getId()], ['id' => 'asc']); 
        
        if(isset($dateLast) && !empty($dateLast) && isset($dateFirst) && !empty($dateFirst)){
            if($dateLast === $dateFirst){

                $last = $dateLast->getStart();
                $end = $dateLast->getEnd();

                $event = $eventRepository->findOneBy(['member' => $this->getUser(), 'start' => $last, 'end' => $end]);
                
                $startDate = $event->getStart();
                $startDate = $startDate->format("d-m-Y H:i:s");
        
                return $this->render('modifyHisBooking/changeBooking.html.twig', [
                'event' => $event, 'startDate' => $startDate]);
            }else{
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->remove($dateFirst);
                $entityManager->flush();

                $event = $eventRepository->findOneBy(['start' => $dateLast->getStart(), 'end' => $dateLast->getEnd(), 'member' => $this->getUser()->getId()]);
    
                $startDate = $event->getStart();
                $startDate = $startDate->format("d-m-Y H:i:s");
        
                return $this->render('modifyHisBooking/changeBooking.html.twig', [
                'event' => $event, 'startDate' => $startDate]);
            }
            
        }else{
            $this->addFlash('error', ' Une erreur est survenue, nous nous excusons pour la gêne occasionnée. Nous vous invitons à sélectionner de nouveau la date à modifier.');

            return $this->redirectToRoute('event_planning_update');
            
        }
    } 

    /**
     * @Route("/user/modify/reservation/date/{idBookingOld}", name="event_update", methods={"GET|POST"})
     */
    public function modifyReservationDate(EventRepository $eventRepository, Event $idBookingOld)
    {
        if(isset($idBookingOld) && !empty($idBookingOld)){
            $events = $eventRepository->findAll();

            $rdvs = [];

            foreach ($events as $event ) {
                $rdvs[] = [
                    'id' => $event->getId(),
                    'title' => $event->getTitle(),
                    'start' => $event->getStart()->format("Y-m-d H:i:s"),
                    'end' => $event->getEnd()->format("Y-m-d H:i:s")
                ];
            }

            $data = json_encode($rdvs);

            return $this->render('modifyHisBooking/modifyReservationDate.html.twig', ['idBookingOld' => $idBookingOld->getId(), 'data' => $data]);
        }
        $this->addFlash('error', ' Une erreur est survenue, nous nous excusons pour la gêne occasionnée. Nous vous invitons à cliquer de nouveau sur le bouton suivant.');

        return $this->redirectToRoute('event_change');
    }

    /**
     * @Route("/user/event/modification/validate/{idBookingOld}", name="event_modification_validate", methods={"GET|POST"})
     */
    public function eventModificationValidate(EventRepository $eventRepository, Event $idBookingOld, DateRepository $dateRepository)
    {
        $start = htmlspecialchars($_GET['start']);
        $end = htmlspecialchars($_GET['end']);

        if(isset($idBookingOld) && !empty($idBookingOld)){
            if(isset($start) && !empty($start) && isset($end) && !empty($end)){
                
                $eventOld = $idBookingOld;
                $startDateEvent = new \dateTime($start);
                $EndDateEvent =  new \dateTime($end);

                $eventNew = $eventOld->setStart($startDateEvent);
                $eventNew = $eventOld->setEnd($EndDateEvent);

                $orm = $this->getDoctrine()->getManager();
                $orm->persist($eventNew);
                $orm->flush();

                $dateLast = $dateRepository->findOneBy(['member' => $this->getUser()->getId()], ['id' => 'desc']);

                $orm = $this->getDoctrine()->getManager();
                $orm->remove($dateLast);
                $orm->flush();

                return $this->redirectToRoute('event_success_update');
            }
        }
    } 

    /**
     * @Route("/user/reservation/date/modification/success/message", name="event_success_update", methods={"GET|POST"})
     */
    public function reservationDateModificationSuccessMessage(EventRepository $eventRepository)
    {
        $this->addFlash('success', ' La modification de votre RDV est réussite !!! ');

        return $this->redirectToRoute('home_page');
    }
} 
