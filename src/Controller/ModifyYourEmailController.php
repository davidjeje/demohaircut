<?php

namespace App\Controller; 

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\UseType;


class ModifyYourEmailController extends AbstractController
{
	/**
     * @Route("/user/update/email", name="update_email_user", methods={"GET|POST"})  
     */
    public function updateEmailUser(Request $request): Response
    {
        $user = $this->getUser();
        
        $form = $this->createForm(UseType::class, $user);
        $form->handleRequest($request); 

        if ($form->isSubmitted() && $form->isValid())  {
            
            $email = $user->getEmail();
            
            $user->setEmail($email);  
            
            $orm = $this->getDoctrine()->getManager();
            $orm->persist($user);
            $orm->flush();

            $this->addFlash('success', ' La modification de votre Email est réussite !!! ');

            return $this->redirectToRoute('home_page');
        }

        return $this->render('modifyYourEmail/updateEmailUser.html.twig', ['user' => $user,
            'form' => $form->createView()]); 
    }
} 