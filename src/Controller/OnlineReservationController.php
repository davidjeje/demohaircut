<?php
//require '../vendor/autoload.php';

namespace App\Controller;

use App\Entity\Haircut;
use App\Form\HaircutType; 
use App\Repository\HaircutRepository;
use App\Entity\Event; 
use App\Form\EventType;
use App\Repository\EventRepository;
use App\Entity\EventNotValidated;
use App\Repository\EventNotValidatedRepository;
use App\Entity\Date;
use App\Repository\DateRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Dompdf\Dompdf;
use Dompdf\Options;

class OnlineReservationController extends AbstractController 
{
    /**
     * @Route("/user/online/booking/{id}", name="online_booking", methods={"GET"})
     */
    public function onlineBooking(EventRepository $eventRepository, $id): Response
    {
        $events = $eventRepository->findAll();

        $rdvs = [];

        foreach ($events as $event ) {
            $rdvs[] = [
                'id' => $event->getId(),
                'title' => $event->getTitle(),
                'start' => $event->getStart()->format("Y-m-d H:i:s"),
                'end' => $event->getEnd()->format("Y-m-d H:i:s")
            ];
        }

        $data = json_encode($rdvs);

        return $this->render('onlineReservation/onlineBooking.html.twig', [ 'haircutId' => $id, 'data' => $data]);
    } 

    /**
     * @Route("/user/event/new/{haircutId}", name="event_new", methods={"GET"}) 
     */
    public function new(Request $request, HaircutRepository $haircutRepository, Haircut $haircutId)
    { 
        $title = htmlspecialchars($_GET['title']);
        $start = htmlspecialchars($_GET['start']);
        $end = htmlspecialchars($_GET['end']);
        
        if(isset($haircutId) && !empty($haircutId)){
            if(isset($title) && !empty($title)){
                if(isset($start) && !empty($start)){
                    if(isset($end) && !empty($end)){
                        $startDateEvent = new \dateTime($start);
                        $EndDateEvent =  new \dateTime($end);

                        if($startDateEvent->format("Y") >= date("Y") && $startDateEvent->format("m") >= date("m") ){

                            if($startDateEvent->format("d") === date("d") && $startDateEvent->format("H") >= date("H") && $startDateEvent->format("i") >= date("i")){
                                $eventNotValidated = new EventNotValidated();

                                $eventNotValidated->setTitle($title);
                                $eventNotValidated->setStart($startDateEvent);
                                $eventNotValidated->setEnd($EndDateEvent);
                                $eventNotValidated->setCustomer($this->getUser());
                                $eventNotValidated->setHaircut($haircutId);

                                $entityManager = $this->getDoctrine()->getManager();
                                $entityManager->persist($eventNotValidated);
                                $entityManager->flush();

                                return $this->redirectToRoute('event_order_summary');
                            }
                            elseif($startDateEvent->format("d") > date("d") ){
                                $eventNotValidated = new EventNotValidated();

                                $eventNotValidated->setTitle($title);
                                $eventNotValidated->setStart($startDateEvent);
                                $eventNotValidated->setEnd($EndDateEvent);
                                $eventNotValidated->setCustomer($this->getUser());
                                $eventNotValidated->setHaircut($haircutId);

                                $entityManager = $this->getDoctrine()->getManager(); 
                                $entityManager->persist($eventNotValidated);
                                $entityManager->flush();

                                return $this->redirectToRoute('event_order_summary');
                            }else{
                                throw $this->createNotFoundException('Une erreur s\'est produite');
                            }
                        }else{
                           
                            throw $this->createNotFoundException('start date not found');
                        }         
                    }else{
                        throw $this->createNotFoundException('End date not found');
                    }   
                }else{
                    throw $this->createNotFoundException('Start date not found');
                }   
            }else{
                throw $this->createNotFoundException('Title not found');
            }   
        }else{
            throw $this->createNotFoundException('Haircut not found');
        }      
    } 

    /**
     * @Route("/user/order/summary", name="event_order_summary", methods={"GET"})
     */ 
    public function order(EventNotValidatedRepository $eventNotValidatedRepository): Response
    {  
        $lastUserEvent = $eventNotValidatedRepository->findOneBy(['customer' => $this->getUser()->getId()], ['id' => 'desc']);
        $firstUserEvent = $eventNotValidatedRepository->findOneBy(['customer' => $this->getUser()->getId()], ['id' => 'asc']);
 
        if(!empty($lastUserEvent) && $firstUserEvent === $lastUserEvent){
            $start = $lastUserEvent->getStart();
            
            $start = $start->format("d-m-Y H:i:s");
             
            return $this->render('onlineReservation/orderSummary.html.twig', [
            'event' => $lastUserEvent, 'start' =>$start
            ]);    
        }elseif(!empty($lastUserEvent) && $firstUserEvent !== $lastUserEvent){

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($firstUserEvent);
            $entityManager->flush();

            $start = $lastUserEvent->getStart();
            
            $start = $start->format("d-m-Y H:i:s");
             
            return $this->render('onlineReservation/orderSummary.html.twig', [
            'event' => $lastUserEvent, 'start' =>$start
            ]);    
        }else{
            $this->addFlash('error', ' Une erreur s\'est produite, sélectionner de nouveau la coupe de cheveux que vous souhaitez avec un créneau horaire dans les heures ou jours à venir. Merci.');

            return $this->redirectToRoute('home_page');
        }    
    } 

    /**
     * @Route("/user/online/payment", name="event_payment", methods={"GET"}) 
     */
    public function payment(Request $request, HaircutRepository $haircutRepository, EventNotValidatedRepository $eventNotValidatedRepository)
    { 
        $lastUserEvent = $eventNotValidatedRepository->findOneBy(['customer' => $this->getUser()->getId()], ['id' => 'desc']);
         
        if(isset($lastUserEvent) && !empty($lastUserEvent)){

            $haircutPrice = $lastUserEvent->getHaircut()->getPrice();
        
            //require_once('../vendor/autoload.php'); 

            \Stripe\Stripe::setApiKey('sk_test_51IUKvkAez1VhFKwvtERCpj2IulQqmxVUCMx9sps8QNf0AhUoYZuoErBOClKD0hLhMJaDC6Quu67B6j8JGUEdf2tk00nVCIAkAY');

            $intent = \Stripe\PaymentIntent::create([
                'amount' => $haircutPrice*100,
                'currency' => 'eur'
            ]);

            return $this->render('onlineReservation/onlinePayment.html.twig', [ 'intent' => $intent]);   
        }else{
            return $this->redirectToRoute('event_order_summary');
        }                   
    }
 
    /**
     * @Route("/user/event/add", name="event_add", methods={"GET"})  
     */
    public function eventAdd(Request $request, EventRepository $eventRepository, EventNotValidatedRepository $eventNotValidatedRepository): Response
    {
        $lastUserEvent = $eventNotValidatedRepository->findOneBy(['customer' => $this->getUser()->getId()], ['id' => 'desc']);

        if(isset($lastUserEvent) && !empty($lastUserEvent)){

            $event = new Event();
            $event->setTitle($lastUserEvent->getTitle());
            $event->setStart($lastUserEvent->getStart());
            $event->setEnd($lastUserEvent->getEnd());
            $event->setMember($lastUserEvent->getCustomer());
            $event->setHaircut($lastUserEvent->getHaircut());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($event);
            $entityManager->flush();

            $eventNewRegistrer = $eventRepository->findOneBy(['member' => $this->getUser()->getId()], ['id' => 'desc']);
            

            if($lastUserEvent->getStart() === $eventNewRegistrer->getStart() && $lastUserEvent->getEnd() === $eventNewRegistrer->getEnd()){
                $entityManager->remove($lastUserEvent);
                $entityManager->flush();

                $dateOfDay = date("d-m-Y");
                
                return $this->render('onlineReservation/bill.html.twig', ['eventNewRegistrer' => $eventNewRegistrer,'dateOfDay' => $dateOfDay]);
            }           
        }
        elseif(empty($lastUserEvent)){
            $eventNewRegistrer = $eventRepository->findOneBy(['member' => $this->getUser()->getId()], ['id' => 'desc']);
            $dateOfDay = date("d-m-Y");
            return $this->render('onlineReservation/bill.html.twig', ['eventNewRegistrer' => $eventNewRegistrer,'dateOfDay' => $dateOfDay]);
        }
    }
 
    /**
     * @Route("/user/bill", name="user_bill_upload", methods={"GET"}) 
     */
    public function billUpload(Request $request, EventRepository $eventRepository): Response
    {

        $eventNewRegistrer = $eventRepository->findOneBy(['member' => $this->getUser()->getId()], ['id' => 'desc']);
        $dateOfDay = date("d-m-Y");
         
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Arial');
        
        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);
        
        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('onlineReservation/uploadBill.html.twig', [
            'eventNewRegistrer' => $eventNewRegistrer,'dateOfDay' => $dateOfDay
        ]);
        
        // Load HTML to Dompdf
        $dompdf->loadHtml($html); 
        
        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        return new Response($dompdf->stream("mypdf.pdf", [
            "Attachment" => true
        ]));     
    }       
} 
