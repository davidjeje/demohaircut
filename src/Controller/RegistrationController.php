<?php

namespace App\Controller;
 
use App\Entity\User;
use App\Form\RegistrationFormType;
use App\Security\EmailVerifier;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use SymfonyCasts\Bundle\VerifyEmail\Exception\VerifyEmailExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use App\Repository\UserRepository;

class RegistrationController extends AbstractController
{ 
    /** 
     * @Route("/member/register", name="member_register", methods={"GET|POST"}) 
     */
    public function memberRegister(Request $request, UserPasswordHasherInterface $passwordEncoder, MailerInterface $mailer): Response
    { 
        $user = new User(); 
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $roles = ['ROLE_USER'];
            $submittedToken = $request->request->get('token');
            //$password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $password = $passwordEncoder->hashPassword($user, $user->getPassword());
            
            $user->setToken($submittedToken);
            $user->setPassword($password);
            $user->setRoles($roles);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $emailUser = $user->getEmail();

            $email = (new TemplatedEmail())
            ->from('dada.pepe.alal@gmail.com')
            ->to(new Address($emailUser))
            ->subject('Valider votre inscription')

            // path of the Twig template to render
            ->htmlTemplate('registration/confirmationEmailMember.html.twig')

            // pass variables (name => value) to the template
            ->context([
            'submittedToken' => $submittedToken,
            ]);
            try {
                $mailer->send($email);
            } catch (TransportExceptionInterface $e) {
                // some error prevented the email sending; display an
                //'error message or try to resend the message'
            };

            $this->addFlash('success', ' Félicitations ! Votre compte a bien été enregistré. Vous venez de recevoir un email de notre part. Pour finaliser l\'inscription, il faut vous rendre dans votre boîte email et cliquer sur le lien. Cordialement.');

            return $this->redirectToRoute('home_page');
        } 

        return $this->render('registration/memberRegister.html.twig', [
            'registrationForm' => $form->createView(),
            'user' => $user,
        ]);
    }  

    /** 
     * @Route("/admi/register", name="admin_register", methods={"GET|POST"})
     */
    public function adminRegister(Request $request, UserPasswordHasherInterface $passwordEncoder, MailerInterface $mailer): Response
    {
        $user = new User(); 
        $form = $this->createForm(RegistrationFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $roles = ['ROLE_ADMIN'];
            $submittedToken = $request->request->get('token');
                        
            $password = $passwordEncoder->hashPassword($user, $user->getPassword());
            
            $user->setToken($submittedToken);
            $user->setPassword($password);
            $user->setRoles($roles);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $emailUser = $user->getEmail();

            $email = (new TemplatedEmail())
            ->from(new Address($emailUser))
            ->to('dada.pepe.alal@gmail.com')
            ->subject('Valider ou non l\'inscription d\'un admin')

            // path of the Twig template to render
            ->htmlTemplate('registration/confirmationEmailAdmin.html.twig')

            // pass variables (name => value) to the template
            ->context([
            'submittedToken' => $submittedToken,
            ]);
            try {
                $mailer->send($email);
            } catch (TransportExceptionInterface $e) {
                // some error prevented the email sending; display an
                //'error message or try to resend the message'
            };

            $this->addFlash('success', ' Félicitations ! Votre compte a bien été enregistré. Pour finaliser l\'inscription, il faut que l\'administration valide votre demande. Si votre compte n\'est pas actif dans 48H, c\'est que malheuresement votre demande n\'est pas acceptée. Cordialement.');

            return $this->redirectToRoute('home_page');
        } 

        return $this->render('registration/adminRegister.html.twig', [
            'registrationForm' => $form->createView(),
            'user' => $user,
        ]);
    }  
   
    /**
     * @Route("/validate/account/Member/{submittedToken}", name="validate_account_member", methods="GET|POST")
     */
    public function validateAccountMember(UserRepository $userRepository, Request $request, $submittedToken)
    {     
        $user = $userRepository->findOneBy(['token' => $submittedToken]);

        //Son compte utilisateur est actif.
        $user->setIsVerified(true);
        
        $orm = $this->getDoctrine()->getManager();
        $orm->persist($user);
        $orm->flush();
        
        $this->addFlash('success', 'Vous venez de valider votre compte. Vous pouvez vous connectez sur votre espace membre. À bientôt !!!');

        return $this->redirectToRoute('home_page');
    }

    /**
     * @Route("/validate/account/admin/{submittedToken}", name="validate_account_admin", methods="GET|POST")
     */
    public function validateAccountAdmin(UserRepository $userRepository, Request $request, $submittedToken, MailerInterface $mailer)
    {     
        $user = $userRepository->findOneBy(['token' => $submittedToken]);

        //Son compte utilisateur est actif.
        $user->setIsVerified(true);
        
        $orm = $this->getDoctrine()->getManager();
        $orm->persist($user);
        $orm->flush();

        $emailUser = $user->getEmail();

        $email = (new TemplatedEmail())
        ->from('dada.pepe.alal@gmail.com')
        ->to(new Address($emailUser))
        ->subject('Inscription valider')

        // path of the Twig template to render
        ->htmlTemplate('registration/informAdministratorAcountValidate.html.twig')
 
        // pass variables (name => value) to the template
        ->context([
            'submittedToken' => $submittedToken,
            ]);
        try {
            $mailer->send($email);
        } catch (TransportExceptionInterface $e) {
                // some error prevented the email sending; display an
                //'error message or try to resend the message'
        };
        
        $this->addFlash('success', 'Vous venez de valider ce compte. Dorénavant, cette personne peut se connecter sur son espace admin. À bientôt !!!');

        return $this->redirectToRoute('home_page');
    }
 
    /**
     * @Route("/delete/account/admin/{submittedToken}", name="delete_account_admin", methods="GET|POST")
     */
    public function deleteAccountAdmin(UserRepository $userRepository, Request $request, $submittedToken, MailerInterface $mailer)
    {
        
        $user = $userRepository->findOneBySomeField($submittedToken);

        $emailUser = $user->getEmail();
        $email = (new TemplatedEmail())
            ->from('dada.pepe.alal@gmail.com')
            ->to(new Address($emailUser))
            ->subject('l\'inscription n\' est pas accepté')

            // path of the Twig template to render
            ->htmlTemplate('registration/confirmationEmailAdminRegisterNoValidate.html.twig')

            // pass variables (name => value) to the template
            ->context([
            'submittedToken' => $submittedToken,
            ]);
            try {
                $mailer->send($email);
            } catch (TransportExceptionInterface $e) {
                // some error prevented the email sending; display an
                //'error message or try to resend the message'
            };
        
        $orm = $this->getDoctrine()->getManager();
        $orm->remove($user);
        $orm->flush();
        
        $this->addFlash('success', ' Vous n\'avez pas validé le compte administrateur de cette personne.');

        return $this->redirectToRoute('home_page'); 
    }
} 
