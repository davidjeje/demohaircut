<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use App\Entity\Haircut;  
use App\Form\HaircutType;
use App\Repository\HaircutRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
 
class RoadAccessibleToAllController extends AbstractController
{
    /**
     * @Route("/", name="home_page", methods={"GET"})
     */
    public function homePage(HaircutRepository $haircutRepository, Request $request): Response
    {
        return $this->render('roadAccessibleToAll/homePage.html.twig', [
            'haircuts' => $haircutRepository->findAll()
        ]);
    }

    /**
     * @Route("/all/haircuts/{page}", name="all_haircuts", methods={"GET"})
     */
    public function allHaircuts(HaircutRepository $haircutRepository, $page): Response
    {
        $nombreMaxParPage = 6;
        $nombreMax = 6;
        $firstResult = ($page-1) * $nombreMaxParPage; 

        $serviceNumber = $haircutRepository->haircutNumber($firstResult, $nombreMax);

        $findAllPage = $haircutRepository->findAllPage($page, $nombreMaxParPage);
        
        $pagination = array(
            'page' => $page,
            'nbPages' => ceil(count($findAllPage) / $nombreMaxParPage),
            'nomRoute' => 'all_haircuts',
            'paramsRoute' => array()
        );
        return $this->render('roadAccessibleToAll/allHaircuts.html.twig', [
            'serviceNumber' => $serviceNumber, 'pagination' => $pagination]);
    } 

    /**
     * @Route("/haircut/presentation/{id}", name="haircut_presentation", methods={"GET"})
     */
    public function haircutPresentation(Haircut $haircut): Response
    {
        return $this->render('roadAccessibleToAll/haircutPresentation.html.twig', [
            'haircut' => $haircut
        ]);
    }

    /**
     * @Route("/about/of/company", name="about_of_company", methods={"GET"}) 
     */
    public function aboutOfCompany(): Response
    {
        return $this->render('roadAccessibleToAll/aboutOfCompany.html.twig');
    }
 
    /** 
     * @Route("/member/sign/up", name="member_sign_up", methods={"GET|POST"})
     */
    /*public function memberSignUp(Request $request, UserPasswordHasherInterface $passwordEncoder): Response
    { 
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $roles = ['ROLE_USER'];
            $submittedToken = $request->request->get('token');
            //dd($submittedToken);
             
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            
            $user->setIsActive(false);
            $user->setToken($submittedToken);
            $user->setPassword($password);
            $user->setRoles($roles);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            return $this->redirectToRoute('user_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('roadAccessibleToAll/memberSignUp.html.twig', [
            'user' => $user,
            'form' => $form,
        ]); 
    }

    /**
     * @Route("/sign/up/admin", name="admin_sign_up", methods={"GET|POST"}) 
     */
    /*public function adminSignUp(Request $request, UserPasswordHasherInterface $passwordEncoder, /*MailerInterface $mailer): Response
    {  
        $user = new User(); 
        $form = $this->createForm(UserType::class, $user); 
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            
            //$submittedToken = $request->request->get('token');
            $password = $passwordEncoder->encodePassword($user, $user->getPassword());
            $roles = ['ROLE_USER'];

            $user->setPassword($password);  
            $user->setRoles($roles);
            //$user->setIsActive(false);
            //$user->setToken($submittedToken);
            
            $orm = $this->getDoctrine()->getManager();
            $orm->persist($user);
            $orm->flush();

            $emailUser = $user->getEmail();

            $email = (new TemplatedEmail())
            ->from(new Address($emailUser))
            ->to('dada.pepe.alal@gmail.com')
            ->subject('Valider l\'inscription d\'un administrateur')

            // path of the Twig template to render
            ->htmlTemplate('user/mailAdmin.html.twig')

            // pass variables (name => value) to the template
            ->context([
            'submittedToken' => $submittedToken,
            ]);
            try {
                $mailer->send($email);
            } catch (TransportExceptionInterface $e) {
                // some error prevented the email sending; display an
                //'error message or try to resend the message'
            };
            
            $this->addFlash('success', ' Félicitations ! Votre compte a bien été enregistré. Pour finaliser l\'inscription, il faut que l\'administration valide votre demande. Si votre compte n\'est pas actif dans 48H, c\'est que malheuresement votre demande n\'est pas acceptée. Cordialement.');

            return $this->redirectToRoute('home_page');
        }
        
        return $this->render('roadAccessibleToAll/adminSignUp.html.twig', [ 'user' => $user,
            'form' => $form->createView()]); 
    }
 
    /**
     * @Route("/member/sign/in", name="member_sign_in", methods="GET|POST")
     */
    /*public function memberSignIn(Request $request, UserPasswordHasherInterface $passwordEncoder, AuthenticationUtils $authenticationUtils):Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();    

        return $this->render('roadAccessibleToAll/memberSignIn.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    } 
 
    /**
     * @Route("/admin/sign/in", name="admin_sign_in", methods="GET|POST")
     */
    /*public function adminSignIn(Request $request, UserPasswordHasherInterface $passwordEncoder, AuthenticationUtils $authenticationUtils):Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();    

        return $this->render('roadAccessibleToAll/adminSignIn.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    } */
} 

