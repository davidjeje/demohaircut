<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\LoginLink\LoginLinkHandlerInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;



class SecurityController extends AbstractController
{
 
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
         
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);          
    }

    /**
     * @Route("/connection/link", name="connectionLink", methods="GET|POST")
     */
    public function connectionLink(Request $request)
    {
        return $this->render('security/connectionLink.html.twig');
    }

    /**
     * @Route("/login_check", name="login_check")
     */
    public function check()
    {
        throw new \LogicException('This code should never be reached');
    }

    /**
     * @Route("/login/link", name="login_link", methods="GET|POST")
     */
    public function requestLoginLink(LoginLinkHandlerInterface $loginLinkHandler, UserRepository $userRepository, Request $request, MailerInterface $mailer)
    {
        // check if login form is submitted
        if ($request->isMethod('POST')) {
            // load the user in some way (e.g. using the form input)
            $emailUser = $request->request->get('email');
            $user = $userRepository->findOneBy(['email' => $emailUser]);
            
            if($user !== NULL){
                // create a login link for $user this returns an instance
                // of LoginLinkDetails
                $loginLinkDetails = $loginLinkHandler->createLoginLink($user);
                //dd($loginLinkDetails);
                $loginLink = $loginLinkDetails->getUrl();

                $email = (new Email())
                ->from('dada.pepe.alal@gmail.com')
                ->to($emailUser)
                ->subject('Se connecter')
                ->text('Clique sur le lien pour te connecter:' . $loginLink);

                try {
                    $mailer->send($email);
                } catch (TransportExceptionInterface $e) {
                // some error prevented the email sending; display an
                //'error message or try to resend the message'
                };

                $this->addFlash('success', ' Vous venez de recevoir un mail avec un lien de connexion. Veuillez vous rendre dans votre boîte mail pour vous connectez. Merci à bientôt !!!');

                return $this->redirectToRoute('home_page');
            }else{
                $this->addFlash('error', ' Vous avez certainement fait une faute de frappe lorsque vous avez renseigné votre mail. Veuillez réhitérer, Merci à bientôt !!!');
                return $this->redirectToRoute('connectionLink');
            }

        }

        return $this->render('security/login.html.twig');
    }

    /**
     * @Route("/logout", name="app_logout", methods="GET|POST")
     */
    public function logout(): Response
    {
        
    }
}
