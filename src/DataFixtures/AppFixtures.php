<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use App\Entity\User;
use App\Entity\Haircut;
use App\Entity\Event;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @codeCoverageIgnore 
 */
class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
 
        $user = new User();
        $user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setName($faker->name());
		$user->setToken($faker->ean13());
		$user->setNumber($faker->phoneNumber());
		$user->setIsVerified(true);

        $password = $this->encoder->hashPassword($user, 'testUnit');
        $user->setPassword($password);

        $manager->persist($user);

        for ($i = 0; $i < 10; $i++) 
        {
            $haircut = new Haircut();

            $haircut->setName($faker->name());
		    $haircut->setDescription($faker->sentence());
		    $haircut->setPrice($faker->numberBetween(10, 20));
		    $haircut->setImage($faker->imageUrl(700, 500, 'animal', true));

            $manager->persist($haircut);
        }

        for ($i = 0; $i < 10; $i++) 
        {
            $event = new Event();

            $event->setTitle($faker->word());
		    $event->setStart($faker->dateTime('d/m/Y'));
		    $event->setEnd($faker->dateTime('d/m/Y'));
            $event->setHaircut($haircut);
            $event->setMember($user);

            $manager->persist($event);
        }
        /*$haircut = new Haircut();

        $haircut->setName('news haircut');
		$haircut->setDescription($faker->sentence());
		$haircut->setPrice($faker->numberBetween(10, 20));
		$haircut->setImage('BuzzCut+Contours.jpg');*/

        $manager->flush();
    }
}
