<?php
// src/EventSubscriber/AddUserEasyAdminSubscriber
namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
use App\Entity\User;

/**
 * @codeCoverageIgnore 
 */
class AddUserEasyAdminSubscriber implements EventSubscriberInterface
{
    private $encoder;

    public function __construct(UserPasswordHasherInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public static function getSubscribedEvents()
    {
        return [
            // 'kernel.terminate'
            BeforeEntityPersistedEvent::class => [
                'setUserPasswordRole'
            ]
        ];
    }

    public function setUserPasswordRole(BeforeEntityPersistedEvent $event)
    {
        $entity = $event->getEntityInstance();

        if(!($entity instanceof User)){
            return;
        }

        //$password = $entity->getPassword();
        $passwordEncode = $this->encoder->hashPassword($entity, $entity->getPassword());
        $entity->setPassword($passwordEncode);
        $roles = ['ROLE_USER'];
        $entity->setRoles($roles);
    }
} 