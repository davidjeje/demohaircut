<?php
  
namespace App\Tests\Functional\CancelYourReservationController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;

class UserDeletionReservationDateFunctionalTest extends WebTestCase
{
    public function testShouldDisplayUserDeletionReservationDate()
    {
        $client = static::createClient();
        /*$crawler = $client->request('GET', '/login');

        $buttonCrawler = $crawler->selectButton('Connexion');
        $form = $buttonCrawler->form();

        $form = $buttonCrawler->form([
            'email' => 'dadi521987@gmail.com',
            'password' => 'dadilarose',
        ]);
        
        $client->submit($form);
        
        $client->followRedirect();*/
        $userRepository = static::getContainer()->get(UserRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('dadi521987@gmail.com');

        // simulate $testUser being logged in
        $client->loginUser($testUser);

        $client->request('GET', '/user/deletion/reservation/date');     
        //$this->assertSelectorTextContains('h1', 'Cliquez sur la date à supprimer'); 
        $this->assertSelectorTextContains('h1', 'Cheveux et Barbe');
       
        //echo $client->getResponse()->getContent();     
    } 
}   