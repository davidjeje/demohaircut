<?php
  
namespace App\Tests\Functional\CancelYourReservationController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;

class UserEventDataToDeleteFunctionalTest extends WebTestCase
{
    public function testShouldDisplayUserEventDataToDelete()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('dadi521987@gmail.com');

        // simulate $testUser being logged in
        $client->loginUser($testUser);
        
        $_GET['start'] = "2021-09-30 08:00:00"; 
        $_GET['end'] = "2021-09-30 08:30:00";

        $client->request('GET', '/user/event/data/to/delete');     
        $client->followRedirect();
        $client->followRedirect();

        $this->assertSelectorTextContains('h2', 'L\'annulation de votre RDV est réussite !!!');
        
        //echo $client->getResponse()->getContent();   
    }
}   