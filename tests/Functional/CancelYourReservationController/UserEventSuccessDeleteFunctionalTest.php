<?php
  
namespace App\Tests\Functional\CancelYourReservationController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserEventSuccessDeleteFunctionalTest extends WebTestCase
{
    public function testShouldDisplayUserEventSuccessDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/login');

        $buttonCrawler = $crawler->selectButton('Connexion');
        $form = $buttonCrawler->form();

        $form = $buttonCrawler->form([
            'email' => 'dadi521987@gmail.com',
            'password' => 'dadilarose',
        ]);
        
        $client->submit($form);
        
        $client->followRedirect();

        $client->request('GET', '/user/event/success/delete');  
        $client->followRedirect();
        $this->assertSelectorTextContains('h2', 'L\'annulation de votre RDV est réussite !!!');
        //echo $client->getResponse()->getContent();
    } 
}  