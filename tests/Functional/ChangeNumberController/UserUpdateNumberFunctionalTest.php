<?php
  
namespace App\Tests\Functional\ChangeNumberController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserUpdateNumberFunctionalTest extends WebTestCase
{
    public function testShouldDisplayUserUpdateNumber()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $buttonCrawler = $crawler->selectButton('Connexion');
        $form = $buttonCrawler->form();

        $form = $buttonCrawler->form([
            'email' => 'dadi521987@gmail.com',
            'password' => 'dadilarose',
        ]);
        
        $client->submit($form);
        
        $client->followRedirect();
        //$this->assertSelectorTextContains('h1', 'Cheveux et Barbe');
        //echo $client->getResponse()->getContent();
        $crawler = $client->request('GET', '/user/update/number');
        //echo $client->getResponse()->getContent();
        $form = $crawler->selectButton('Modifier')->form();
        $form['us[number]'] = '0678894543';
        
        $client->submit($form);
        //echo $client->getResponse()->getContent();
        $client->followRedirect();
        //echo $client->getResponse()->getContent();
        $this->assertSelectorTextContains('h2', 'La modification de votre numéro de téléphone est réussite !!!');
        //echo $client->getResponse()->getContent();
    }
}   