<?php
  
namespace App\Tests\Functional\ChangePasswordController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserUpdatePasswordFunctionalTest extends WebTestCase
{
    public function testShouldDisplayUserUpdatePassword()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $buttonCrawler = $crawler->selectButton('Connexion');
        $form = $buttonCrawler->form();

        $form = $buttonCrawler->form([
            'email' => 'dadi521987@gmail.com',
            'password' => 'dadilarose',
        ]);
        
        $client->submit($form);
        
        $client->followRedirect(); 
        
        $client->request('GET', '/user/update/password'); 

        $client->submitForm('Modifier', [
            'users[password][first]' => 'dadilarose',
            'users[password][second]' => 'dadilarose',
        ]);
        $client->followRedirect();
            
        $this->assertSelectorTextContains('h2', 'La modification de votre mot de passe est réussite !!! Dorénavant connectez-vous avec ce mot de passe.');
        //echo $client->getResponse()->getContent();
    }
}  