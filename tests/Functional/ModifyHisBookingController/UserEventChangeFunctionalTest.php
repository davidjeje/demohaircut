<?php
  
namespace App\Tests\Functional\ModifyHisBookingController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;

class UserEventChangeFunctionalTest extends WebTestCase
{
    public function testShouldDisplayUserEventChange()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('dadi521987@gmail.com');

        // simulate $testUser being logged in
        $client->loginUser($testUser);

        $client->request('GET', '/user/event/change');     

        $this->assertSelectorTextContains('h3', 'Pour modifier ce RDV appuyer sur le bouton valider'); 
        
        //echo $client->getResponse()->getContent();
    }
}    