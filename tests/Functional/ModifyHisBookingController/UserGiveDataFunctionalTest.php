<?php
  
namespace App\Tests\Functional\ModifyHisBookingController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;

class UserGiveDataFunctionalTest extends WebTestCase
{
    public function testShouldDisplayUserGiveData()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('dadi521987@gmail.com');

        // simulate $testUser being logged in
        $client->loginUser($testUser);

        $_GET['start'] = "2021-09-29 09:00:00";
        $_GET['end'] = "2021-09-29 09:30:00";

        $client->request('GET', '/user/event/give/data'); 
        $client->followRedirect();    
        
        $this->assertSelectorTextContains('h3', 'Pour modifier ce RDV appuyer sur le bouton valider');

        //echo $client->getResponse()->getContent();
    }
}    