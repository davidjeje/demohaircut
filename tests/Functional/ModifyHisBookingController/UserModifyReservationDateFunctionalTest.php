<?php
  
namespace App\Tests\Functional\ModifyHisBookingController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;

class UserModifyReservationDateFunctionalTest extends WebTestCase
{
    public function testShouldDisplayUserModifyReservationDate()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('dadi521987@gmail.com');

        // simulate $testUser being logged in
        $client->loginUser($testUser);

        $client->request('GET', '/user/modify/reservation/date/39');   
        $this->assertSelectorTextContains('h1', 'Cheveux et Barbe'); 
        //echo $client->getResponse()->getContent();
    } 
}  