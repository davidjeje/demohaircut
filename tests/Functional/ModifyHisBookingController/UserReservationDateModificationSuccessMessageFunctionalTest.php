<?php
  
namespace App\Tests\Functional\ModifyHisBookingController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;

class UserReservationDateModificationSuccessMessageFunctionalTest extends WebTestCase
{
    public function testShouldDisplayUserUserReservationDateModificationSuccessMessage()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('dadi521987@gmail.com');

        // simulate $testUser being logged in
        $client->loginUser($testUser);

        $client->request('GET', '/user/reservation/date/modification/success/message');     
        $client->followRedirect();

        $this->assertSelectorTextContains('h2', 'La modification de votre RDV est réussite !!!');
        //echo $client->getResponse()->getContent();
    }
}  