<?php
  
namespace App\Tests\Functional\ModifyHisBookingController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;

class UserSelectBookingDateModifyFunctionalTest extends WebTestCase
{
    public function testShouldDisplayUserSelectBookingDateModify()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('dadi521987@gmail.com');

        // simulate $testUser being logged in
        $client->loginUser($testUser);

        $client->request('GET', '/user/select/booking/date/modify');    
        $this->assertSelectorTextContains('h1', 'Cheveux et Barbe');
        //echo $client->getResponse()->getContent();
    }
}     