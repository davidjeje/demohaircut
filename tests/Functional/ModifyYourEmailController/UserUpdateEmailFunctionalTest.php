<?php
  
namespace App\Tests\Functional\ModifyYourEmailController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserUpdateEmailFunctionalTest extends WebTestCase
{
    public function testShouldDisplayUserUpdateEmail()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $buttonCrawler = $crawler->selectButton('Connexion');
        $form = $buttonCrawler->form();

        $form = $buttonCrawler->form([
            'email' => 'dadi521987@gmail.com',
            'password' => 'dadilarose',
        ]);
        
        $client->submit($form);
        
        $client->followRedirect(); 

        $client->request('GET', '/user/update/email'); 

        $client->submitForm('Modifier', [
            'use[email]' => 'dadi521987@gmail.com',
        ]);
        $client->followRedirect();
            
        $this->assertSelectorTextContains('h2', 'La modification de votre Email est réussite !!!');
        //echo $client->getResponse()->getContent();
    }
}     