<?php
  
namespace App\Tests\Functional\OnlineReservationController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserBillFunctionalTest extends WebTestCase
{
    public function testShouldDisplayUserBill()
    {
        $client = static::createClient();

        $client->request('GET', '/user/bill');
        //$this->assertResponseIsSuccessful();      
        $this->assertSame(302, $client->getResponse()->getStatusCode());
    }
}