<?php
  
namespace App\Tests\Functional\OnlineReservationController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;

class UserEventAddFunctionalTest extends WebTestCase
{
    public function testShouldDisplayUserEventAdd()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('dadi521987@gmail.com');

        // simulate $testUser being logged in
        $client->loginUser($testUser);

        $client->request('GET', '/user/event/add');
        $this->assertSelectorTextContains('h1', 'Cheveux et Barbe');

        //echo $client->getResponse()->getContent(); 
    }
} 