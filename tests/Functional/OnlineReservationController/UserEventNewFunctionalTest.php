<?php
  
namespace App\Tests\Functional\OnlineReservationController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use App\Repository\UserRepository;

class UserEventNewFunctionalTest extends WebTestCase
{
    public function testShouldDisplayUserEventNew()
    {
        $client = static::createClient();

        $userRepository = static::getContainer()->get(UserRepository::class);

        // retrieve the test user
        $testUser = $userRepository->findOneByEmail('dadi521987@gmail.com');

        // simulate $testUser being logged in
        $client->loginUser($testUser);

        $_GET['title'] = "Réservé";
        $_GET['start'] = "2021-09-30 08:00:00";
        $_GET['end'] = "2021-09-30 08:30:00";


        $client->request('GET', '/user/event/new/26');
        $client->followRedirect();
        $this->assertSelectorTextContains('h1', 'Cheveux et Barbe');

        //echo $client->getResponse()->getContent(); 
    }
} 