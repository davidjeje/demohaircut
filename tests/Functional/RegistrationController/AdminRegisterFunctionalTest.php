<?php
  
namespace App\Tests\Functional\RegistrationController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AdminRegisterFunctionalTest extends WebTestCase
{
    public function testShouldDisplayMemberRegister()
    {
        $client = static::createClient();
        $client->request('GET', '/admi/register');
        $this->assertResponseIsSuccessful();
        
        $this->assertSelectorTextContains('h3', 'Inscription Admin');
        $this->assertSelectorTextContains('p', 'Coiffeur uniquement pour les hommes.');
        $this->assertSelectorTextContains('button', 'S\'inscrire');

        //echo $client->getResponse()->getContent();
    }

    public function testAdminSuccessFormRegister()
    {

        $client = static::createClient(); 
        $crawler = $client->request('GET', '/admi/register');

        $client->submitForm('S\'inscrire', [
            'registration_form[name]' => 'bade',
            'registration_form[email]' => 'marionbade29@gmail.com',
            'registration_form[number]' => '0756654525',
            'registration_form[password][first]' => 'marionBade',
            'registration_form[password][second]' => 'marionBade',
            'registration_form[agreeTerms]' => true,    
        ]);
        $client->followRedirect();
        $this->assertSelectorTextContains('h2', 'Félicitations ! Votre compte a bien été enregistré. Pour finaliser l\'inscription, il faut que l\'administration valide votre demande. Si votre compte n\'est pas actif dans 48H, c\'est que malheuresement votre demande n\'est pas acceptée. Cordialement.');
        
        //echo $client->getResponse()->getContent();      
    } 
  
    public function testFailedFormRegister()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/admi/register');

        $client->submitForm('S\'inscrire', [
            'registration_form[name]' => 'dada',
            'registration_form[email]' => 'dada.pepe.alal@gmail.com',
            'registration_form[number]' => '0756652369',
            'registration_form[password][first]' => 'dadaLarose',
            'registration_form[password][second]' => 'dadaLarose',
            'registration_form[agreeTerms]' => true, 
        ]);
        $this->assertSelectorTextContains('div.alert.alert-danger', 'There is already an account with this email');     
        
        //echo $client->getResponse()->getContent();    
    } 
}   