<?php
  
namespace App\Tests\Functional\RegistrationController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DeleteAccountAdminFunctionalTest extends WebTestCase
{
    public function testShouldDisplayDeleteAccountAdmin()
    {
        $client = static::createClient();

        $client->request('GET', '/delete/account/admin/5');     
        $this->assertSame(500, $client->getResponse()->getStatusCode());
    }
}  