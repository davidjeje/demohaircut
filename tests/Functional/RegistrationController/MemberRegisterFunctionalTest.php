<?php
  
namespace App\Tests\Functional\RegistrationController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MemberRegisterFunctionalTest extends WebTestCase
{
    public function testShouldDisplayMemberRegister()
    {
        $client = static::createClient();
        $client->request('GET', '/member/register');
        $this->assertResponseIsSuccessful();
        
        $this->assertSelectorTextContains('h3', 'Inscription Membre');
        $this->assertSelectorTextContains('p', 'Coiffeur uniquement pour les hommes.');
        $this->assertSelectorTextContains('button', 'S\'inscrire');

        //echo $client->getResponse()->getContent();
    }

    public function testSuccessFormRegister()
    {

        $client = static::createClient(); 
        $crawler = $client->request('GET', '/member/register');

        $client->submitForm('S\'inscrire', [
            'registration_form[name]' => 'marion',
            'registration_form[email]' => 'marion.emi@hotmail.fr',
            'registration_form[number]' => '0756652386',
            'registration_form[password][first]' => 'marionEmi',
            'registration_form[password][second]' => 'marionEmi',
            'registration_form[agreeTerms]' => true,
            
        ]);
        $client->followRedirect();
        $this->assertSelectorTextContains('h2', 'Félicitations ! Votre compte a bien été enregistré. Vous venez de recevoir un email de notre part. Pour finaliser l\'inscription, il faut vous rendre dans votre boîte email et cliquer sur le lien. Cordialement.');
        
        //echo $client->getResponse()->getContent();      
    } 
  
    public function testFailedFormRegister()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/member/register');

        $client->submitForm('S\'inscrire', [
            'registration_form[name]' => 'dada',
            'registration_form[email]' => 'dada.pepe.alal@gmail.com',
            'registration_form[number]' => '0756652369',
            'registration_form[password][first]' => 'dadaLarose',
            'registration_form[password][second]' => 'dadaLarose',
            'registration_form[agreeTerms]' => true, 
        ]);
        $this->assertSelectorTextContains('div.alert.alert-danger', 'There is already an account with this email');     
        
        //echo $client->getResponse()->getContent();    
    }
} 