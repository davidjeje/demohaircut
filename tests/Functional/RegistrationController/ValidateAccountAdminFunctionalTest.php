<?php
  
namespace App\Tests\Functional\RegistrationController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ValidateAccountAdminFunctionalTest extends WebTestCase
{
    public function testShouldDisplayValidateAccountAdmin()
    {
        $client = static::createClient();

        $client->request('GET', '/validate/account/admin/9178390187142');
        
        $this->assertSame(302, $client->getResponse()->getStatusCode());
        
    }
} 