<?php
  
namespace App\Tests\Functional\RegistrationController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ValidateAccountMemberFunctionalTest extends WebTestCase
{
    public function testShouldDisplayValidateAccountMember()
    {
        $client = static::createClient();

        $client->request('GET', '/validate/account/Member/9178390187142');
        $this->assertSame(302, $client->getResponse()->getStatusCode());
    }
}