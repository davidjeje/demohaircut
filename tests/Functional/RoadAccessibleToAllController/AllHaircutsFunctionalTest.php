<?php
  
namespace App\Tests\Functional\RoadAccessibleToAllController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AllHaircutsFunctionalTest extends WebTestCase
{
    public function testShouldDisplayAllHaircuts()
    {
        $client = static::createClient();

        $client->request('GET', '/all/haircuts/1');     
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Cheveux et Barbe');
        $this->assertSelectorTextContains('p', 'Coiffeur uniquement pour les hommes.'); 
    }
}   