<?php
  
namespace App\Tests\Functional\RoadAccessibleToAllController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CompagnyFunctionalTest extends WebTestCase
{
    public function testShouldDisplayAboutOfCompagny()
    {
        $client = static::createClient();
        $client->request('GET', '/about/of/company');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Cheveux et Barbe');
        $this->assertSelectorTextContains('p', 'Coiffeur uniquement pour les hommes.');
        $this->assertSelectorTextContains('h2', 'Demo Hair Coiffure');
    }
} 