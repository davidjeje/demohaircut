<?php
  
namespace App\Tests\Functional\RoadAccessibleToAllController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HaircutPresentationFunctionalTest extends WebTestCase
{
    public function testShouldDisplayHaircutPresentation()
    {
        $client = static::createClient();

        $client->request('GET', '/haircut/presentation/26');     
        $this->assertSame(500, $client->getResponse()->getStatusCode());
    }
}    