<?php
  
namespace App\Tests\Functional\RoadAccessibleToAllController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeFunctionalTest extends WebTestCase
{
    public function testShouldDisplayHomepage()
    {
        $client = static::createClient();
        $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Cheveux et Barbe');
        $this->assertSelectorTextContains('p', 'Coiffeur uniquement pour les hommes.');
        $this->assertSelectorTextContains('h3', 'Prestation du salon');
    }
} 