<?php
  
namespace App\Tests\Functional\SecurityController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ConnectionLinkFunctionalTest extends WebTestCase
{
    public function testShouldDisplayLogin()
    {
        $client = static::createClient();
        $client->request('GET', '/connection/link');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Cheveux et Barbe');
        $this->assertSelectorTextContains('p', 'Coiffeur uniquement pour les hommes.');
        $this->assertSelectorTextContains('button', 'Connexion');
    }
}