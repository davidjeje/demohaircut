<?php
  
namespace App\Tests\Functional\SecurityController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginFunctionalTest extends WebTestCase
{
    public function testShouldDisplayLogin() 
    {
        $client = static::createClient();
        $client->request('GET', '/login');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Cheveux et Barbe');
        $this->assertSelectorTextContains('p', 'Coiffeur uniquement pour les hommes.');
        $this->assertSelectorTextContains('button', 'Connexion');
    }

    public function testFormLogin()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $buttonCrawler = $crawler->selectButton('Connexion');
        $form = $buttonCrawler->form();

        $form = $buttonCrawler->form([
            'email' => 'dadi521987@gmail.com',
            'password' => 'dadilarose',
        ]);

        $client->submit($form);
        //$client->followRedirect();
        $client->request('GET', '/');
        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h1', 'Cheveux et Barbe');        
    } 
} 