<?php
  
namespace App\Tests\Functional\SecurityController;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class LoginLinkFunctionalTest extends WebTestCase
{
    public function testShouldDisplayConnectionLink()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/login');

        $link = $crawler->selectLink('Mot de passe oublié')->link();
        $crawler = $client->click($link);

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        $this->assertSelectorTextContains('h3', 'Se connecter');
        //echo $client->getResponse()->getContent();
    }

    public function testFormFailedConnectionLink()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/connection/link');

        /*$buttonCrawler = $crawler->selectButton('Connexion');

        $form = $buttonCrawler->form([
            'email' => 'test@gmail.com',
        ]);

        $client->submit($form);*/

        $form = $crawler->selectButton('Connexion')->form();
        $form['email'] = 'test@gmail.com';
        
        $crawler = $client->submit($form);

        $client->followRedirect();
        $this->assertSelectorTextContains('h2', 'Vous avez certainement fait une faute de frappe lorsque vous avez renseigné votre mail. Veuillez réhitérer, Merci à bientôt !!!');
         
        //echo $client->getResponse()->getContent();
    } 

    public function testFormSuccessConnectionLink()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/connection/link');

        /*$buttonCrawler = $crawler->selectButton('Connexion');

        $form = $buttonCrawler->form([
            'email' => 'test@gmail.com',
        ]);

        $client->submit($form);*/

        $form = $crawler->selectButton('Connexion')->form();
        $form['email'] = 'dadi521987@gmail.com';
        
        $crawler = $client->submit($form);

        $client->followRedirect();
    
        $this->assertSelectorTextContains('h2', 'Vous venez de recevoir un mail avec un lien de connexion. Veuillez vous rendre dans votre boîte mail pour vous connectez. Merci à bientôt !!!');
         
        //echo $client->getResponse()->getContent();
    } 
}  