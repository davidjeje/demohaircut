<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Date;
use App\Entity\User;

class DateUnitTest extends TestCase
{
	public function testIsTrue()
	{
        $user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setName('test');
		$user->setToken('155665446561');
		$user->setNumber('0654123587');
        $user->setIsVerified(true);
        
        $today = new \dateTime('10/10/21');

        $date = new Date();
        $date->setStart($today);
		$date->setEnd($today);
		$date->setMember($user);

		$this->assertTrue($date->getStart() === $today);
		$this->assertTrue($date->getEnd() === $today);
		$this->assertTrue($date->getMember() === $user);
		
	}

	public function testIsFalse()
	{
        $user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setName('test');
		$user->setToken('155665446561');
		$user->setNumber('0654123587');
        $user->setIsVerified(true);
 
        $user2 = new User();
		$user2->setEmail('test2@gmail.com');
		$user2->setRoles(['ROLE_ADMIN']);
		$user2->setPassword('testUnit2');
		$user2->setName('test2');
		$user2->setToken('155665446569');
		$user2->setNumber('0654123585');
        $user2->setIsVerified(false);
        
        $today = new \dateTime('10/10/21');
        
        $today2 = new \dateTime('10/11/21');

        $date = new Date();
        $date->setStart($today);
		$date->setEnd($today);
		$date->setMember($user);

		$this->assertFalse($date->getStart() === $today2);
		$this->assertFalse($date->getEnd() === $today2);
		$this->assertFalse($date->getMember() === $user2);
	}

	public function testIsEmpty()
	{
		$date = new Date();

		$this->assertEmpty($date->getId());
		$this->assertEmpty($date->getStart());
		$this->assertEmpty($date->getEnd());
		$this->assertEmpty($date->getMember());
	}
}