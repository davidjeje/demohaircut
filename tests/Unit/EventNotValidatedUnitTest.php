<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\EventNotValidated;
use App\Entity\Haircut;
use App\Entity\User;
 
class EventNotValidatedUnitTest extends TestCase
{
	public function testIsTrue()
	{
        $event = new EventNotValidated();

        $haircut = new Haircut();
        $haircut->setName('test');
		$haircut->setDescription('test qui fonctionne');
		$haircut->setPrice(15);
        $haircut->setImage('testUnitImage.jpg');
        
        $date = new \dateTime('10/10/21');

        $user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setName('test');
		$user->setToken('155665446561');
		$user->setNumber('0654123587');
		$user->setIsVerified(true);

        $event->setTitle('test event');
		$event->setStart($date);
		$event->setEnd($date);
        $event->setHaircut($haircut);
        $event->setCustomer($user);

		$this->assertTrue($event->getTitle() === 'test event');
		$this->assertTrue($event->getStart() === $date);
		$this->assertTrue($event->getEnd() === $date);
        $this->assertTrue($event->getHaircut() === $haircut);
        $this->assertTrue($event->getCustomer() === $user);
		
	}

	public function testIsFalse()
	{
        $event = new EventNotValidated();

        $haircut = new Haircut();
        $haircut->setName('test');
		$haircut->setDescription('test qui fonctionne');
		$haircut->setPrice(15);
        $haircut->setImage('testUnitImage.jpg');

        $haircut2 = new Haircut();
        $haircut2->setName('test2');
		$haircut2->setDescription('test qui ne fonctionne pas');
		$haircut2->setPrice(20);
        $haircut2->setImage('testUnitFalseImage.jpg');
        
        $date = new \dateTime('10/10/21');
        $date2 = new \dateTime('11/11/21');

        $user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setName('test');
		$user->setToken('155665446561');
		$user->setNumber('0654123587');
        $user->setIsVerified(true);
        
        $user2 = new User();
		$user2->setEmail('testFalse@gmail.com');
		$user2->setRoles(['ROLE_ADMIN']);
		$user2->setPassword('testUnitFalse');
		$user2->setName('testFalse');
		$user2->setToken('155665446565');
		$user2->setNumber('0654123589');
		$user2->setIsVerified(false);

        $event->setTitle('test event');
		$event->setStart($date);
		$event->setEnd($date);
        $event->setHaircut($haircut);
        $event->setCustomer($user);

		$this->assertFalse($event->getTitle() === 'test event false');
		$this->assertFalse($event->getStart() === $date2);
		$this->assertFalse($event->getEnd() === $date2);
        $this->assertFalse($event->getHaircut() === $haircut2);
        $this->assertFalse($event->getCustomer() === $user2);
	}

	public function testIsEmpty()
	{
		$event = new EventNotValidated();
		
		$this->assertEmpty($event->getId());
		$this->assertEmpty($event->getTitle());
		$this->assertEmpty($event->getStart());
		$this->assertEmpty($event->getEnd());
        $this->assertEmpty($event->getHaircut());
        $this->assertEmpty($event->getCustomer());
	}  
}