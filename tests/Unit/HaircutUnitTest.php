<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Haircut;
use App\Entity\Event;
use App\Entity\EventNotValidated;

class HaircutUnitTest extends TestCase
{
	public function testIsTrue()
	{
        $haircut = new Haircut();
        $haircut->setName('test');
		$haircut->setDescription('test qui fonctionne');
		$haircut->setPrice(15);
		$haircut->setImage('testUnitImage.jpg');

		$this->assertTrue($haircut->getName() === 'test');
		$this->assertTrue($haircut->getDescription() === 'test qui fonctionne');
		$this->assertTrue($haircut->getPrice() === 15);
		$this->assertTrue($haircut->getImage() === 'testUnitImage.jpg');
		
	}

	public function testIsFalse()
	{
        $haircut = new Haircut();
        $haircut->setName('test');
		$haircut->setDescription('test qui fonctionne');
		$haircut->setPrice(15);
		$haircut->setImage('testUnitImage.jpg');

		$this->assertFalse($haircut->getName() === 'test1');
		$this->assertFalse($haircut->getDescription() === 'test qui ne fonctionne pas');
		$this->assertFalse($haircut->getPrice() === 20);
		$this->assertFalse($haircut->getImage() === 'testUnitFalseImage.jpg');
	}

	public function testIsEmpty()
	{
		$haircut = new Haircut();
		
		//$this->assertEmpty($haircut->getId());
		$this->assertEmpty($haircut->getName());
		$this->assertEmpty($haircut->getDescription());
		$this->assertEmpty($haircut->getPrice());
		$this->assertEmpty($haircut->getImage());
		$this->assertEmpty($haircut->getEvents());
		$this->assertEmpty($haircut->getId());
	}

	public function testAddGetRemoveEvent() 
	{
		$haircut = new Haircut();
		$event = new Event();

		$this->assertEmpty($haircut->getEvents());

		$haircut->addEvent($event);
		$this->assertContains($event, $haircut->getEvents());

		$haircut->removeEvent($event);
		$this->assertEmpty($haircut->getEvents());
	}

	public function testAddGetRemoveEventNotValidated()
	{
		$haircut = new Haircut();
		$eventNotValidated = new EventNotValidated();

		$this->assertEmpty($haircut->getEventNotValidated());

		$haircut->addEventNotValidated($eventNotValidated);
		$this->assertContains($eventNotValidated, $haircut->getEventNotValidated());

		$haircut->removeEventNotValidated($eventNotValidated);
		$this->assertEmpty($haircut->getEventNotValidated());
	}
}