<?php

namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Entity\Paginator;

class PaginatorUnitTest extends TestCase
{
	public function testIsTrue()
	{
        $paginator = new Paginator();
        $paginator->setPage(1);
		$paginator->setNbPages(2);
		$paginator->setNomRoute('liste');
		$paginator->setParamsRoute(['ok']);

		$this->assertTrue($paginator->getPage() === 1);
		$this->assertTrue($paginator->getNbPages() === 2);
		$this->assertTrue($paginator->getNomRoute() === 'liste');
		$this->assertTrue($paginator->getParamsRoute() === ['ok']);	
	}

	public function testIsFalse()
	{
        $paginator = new Paginator();
        $paginator->setPage(1);
		$paginator->setNbPages(2);
		$paginator->setNomRoute('liste');
		$paginator->setParamsRoute(['ok']);

		$this->assertFalse($paginator->getPage() === 2);
		$this->assertFalse($paginator->getNbPages() === 3);
		$this->assertFalse($paginator->getNomRoute() === 'liste1');
		$this->assertFalse($paginator->getParamsRoute() === ['okOk']);
	}

	public function testIsEmpty()
	{
		$paginator = new Paginator();
		
		$this->assertEmpty($paginator->getId());
		$this->assertEmpty($paginator->getPage());
		$this->assertEmpty($paginator->getNbPages());
		$this->assertEmpty($paginator->getNomRoute());
		$this->assertEmpty($paginator->getParamsRoute());
	}
}