<?php
  
namespace App\Tests\Unit;

use PHPUnit\Framework\TestCase; 
use App\Entity\User;
use App\Entity\Event;
use App\Entity\EventNotValidated;
use App\Entity\Date;

class UserUnitTest extends TestCase
{
	public function testIsTrue()
	{
		$user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setName('test');
		$user->setToken('155665446561');
		$user->setNumber('0654123587');
		$user->setIsVerified(true);

		$this->assertTrue($user->getEmail() === 'test@gmail.com');
		$this->assertTrue($user->getRoles() === ['ROLE_USER']);
		$this->assertTrue($user->getPassword() === 'testUnit');
		$this->assertTrue($user->getName() === 'test');
		$this->assertTrue($user->getToken() === '155665446561');
		$this->assertTrue($user->getNumber() === '0654123587');
		$this->assertTrue($user->isVerified() === true);
	}

	public function testIsFalse()
	{
		$user = new User();
		$user->setEmail('test@gmail.com');
		$user->setRoles(['ROLE_USER']);
		$user->setPassword('testUnit');
		$user->setName('test');
		$user->setToken('155665446561');
		$user->setNumber('0654123587');
		$user->setIsVerified(true);

		$this->assertFalse($user->getEmail() === 'test2@gmail.com');
		$this->assertFalse($user->getRoles() === ['ROLE_Admin']);
		$this->assertFalse($user->getPassword() === 'testUnit2');
		$this->assertFalse($user->getName() === 'test2');
		$this->assertFalse($user->getToken() === '1556654465612');
		$this->assertFalse($user->getNumber() === '0654123582');
		$this->assertFalse($user->isVerified() === false);
	}

	public function testIsEmpty()
	{
		$user = new User();
		
		$this->assertEmpty($user->getId());
		$this->assertEmpty($user->getSalt());
		$this->assertEmpty($user->getUserIdentifier());
		$this->assertEmpty($user->getUsername());
		$this->assertEmpty($user->getEmail());
		$this->assertNotEmpty($user->getRoles());
		$this->assertEmpty($user->getName());
		$this->assertEmpty($user->getToken());
		$this->assertEmpty($user->getNumber());
		$this->assertEmpty($user->isVerified());
		$this->assertEmpty($user->eraseCredentials());
	}

	public function testAddGetRemoveEvent() 
	{
		$user = new User();
		$event = new Event();

		$this->assertEmpty($user->getEvent());

		$user->addEvent($event);
		$this->assertContains($event, $user->getEvent());

		$user->removeEvent($event);
		$this->assertEmpty($user->getEvent());
	}

	public function testAddGetRemoveEventNotValidated()
	{
		$user = new User();
		$eventNotValidated = new EventNotValidated();

		$this->assertEmpty($user->getEventNotValidated());

		$user->addEventNotValidated($eventNotValidated);
		$this->assertContains($eventNotValidated, $user->getEventNotValidated());

		$user->removeEventNotValidated($eventNotValidated);
		$this->assertEmpty($user->getEventNotValidated());
	}

	public function testAddGetRemoveDate()
	{
		$user = new User();
		$date = new Date();

		$this->assertEmpty($user->getDate());

		$user->addDate($date);
		$this->assertContains($date, $user->getDate());

		$user->removeDate($date);
		$this->assertEmpty($user->getDate());
	}
} 